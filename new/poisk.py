# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'poisk.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(506, 508)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.teg = QtWidgets.QComboBox(self.centralwidget)
        self.teg.setGeometry(QtCore.QRect(10, 10, 111, 31))
        self.teg.setObjectName("teg")
        self.teg.addItem("")
        self.opis = QtWidgets.QComboBox(self.centralwidget)
        self.opis.setGeometry(QtCore.QRect(310, 10, 141, 31))
        self.opis.setObjectName("opis")
        self.opis.addItem("")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(10, 60, 471, 271))
        self.textEdit.setObjectName("textEdit")
        self.textEdit_2 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_2.setGeometry(QtCore.QRect(10, 340, 104, 71))
        self.textEdit_2.setObjectName("textEdit_2")
        self.textEdit_3 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_3.setGeometry(QtCore.QRect(130, 340, 104, 71))
        self.textEdit_3.setObjectName("textEdit_3")
        self.textEdit_4 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_4.setGeometry(QtCore.QRect(250, 340, 104, 71))
        self.textEdit_4.setObjectName("textEdit_4")
        self.textEdit_5 = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit_5.setGeometry(QtCore.QRect(370, 340, 104, 71))
        self.textEdit_5.setObjectName("textEdit_5")
        self.ok = QtWidgets.QPushButton(self.centralwidget)
        self.ok.setGeometry(QtCore.QRect(400, 430, 75, 23))
        self.ok.setObjectName("ok")
        self.vixod = QtWidgets.QPushButton(self.centralwidget)
        self.vixod.setGeometry(QtCore.QRect(10, 430, 75, 23))
        self.vixod.setObjectName("vixod")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 506, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.teg.setItemText(0, _translate("MainWindow", "ПОИСК ПО ТЕГУ"))
        self.opis.setItemText(0, _translate("MainWindow", "ПОИСК ПО ОПИСАНИЮ"))
        self.ok.setText(_translate("MainWindow", "ОК"))
        self.vixod.setText(_translate("MainWindow", "НАЗАД"))
