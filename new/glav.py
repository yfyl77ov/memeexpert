from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(612, 435)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(190, 0, 231, 51))
        self.textEdit.setObjectName("textEdit")
        self.poisk = QtWidgets.QPushButton(self.centralwidget)
        self.poisk.setGeometry(QtCore.QRect(0, 60, 131, 41))
        self.poisk.setObjectName("poisk")
        self.soxr = QtWidgets.QPushButton(self.centralwidget)
        self.soxr.setGeometry(QtCore.QRect(370, 60, 231, 41))
        self.soxr.setObjectName("soxr")
        self.poiskkart = QtWidgets.QTextEdit(self.centralwidget)
        self.poiskkart.setGeometry(QtCore.QRect(0, 100, 191, 151))
        self.poiskkart.setObjectName("poiskkart")
        self.soxrkart = QtWidgets.QTextEdit(self.centralwidget)
        self.soxrkart.setGeometry(QtCore.QRect(300, 100, 301, 151))
        self.soxrkart.setObjectName("soxrkart")
        self.zagryz = QtWidgets.QPushButton(self.centralwidget)
        self.zagryz.setGeometry(QtCore.QRect(0, 260, 151, 41))
        self.zagryz.setObjectName("zagryz")
        self.zagruzkart = QtWidgets.QTextEdit(self.centralwidget)
        self.zagruzkart.setGeometry(QtCore.QRect(170, 280, 421, 111))
        self.zagruzkart.setObjectName("zagruzkart")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 612, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.textEdit.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:24pt; font-weight:600; font-style:italic;\">MEM EXPERT</span></p></body></html>"))
        self.poisk.setText(_translate("MainWindow", "ПОИСК ИЗОБРАЖЕНИЙ"))
        self.soxr.setText(_translate("MainWindow", "ПРОСМОТР СОХРАНЁННЫХ ИЗОБРАЖЕНИЙ"))
        self.zagryz.setText(_translate("MainWindow", "ЗАГРУЗИТЬ ИЗОБРАЖЕНИЕ"))
