import pytest
import PictDBapp


def test_datebase():
    assert PictDBapp.sqlite3_read_pictures_specs_from_db(data_base, table) == data


def test_pict_import():
    assert PictDBapp.sqlite3_simple_pict_import(data_base, table, pict_path, record_id, description) == (record_id, description, pict_path, binary_pict)


def test_export_pict():
    assert PictDBapp.export_pict_from_sql(data_base, table, record_id, path) == write_pict_from_binary(path, pict_binary)


def test_delete_record():
    assert PictDBapp.sqlite3_simple_delete_record(data_base, table, id_column, record_id) == 'DELETE FROM '+table+' WHERE '+id_column+" = '"+record_id+"'"


def test_import_pict():
    assert PictDBapp.import_pict_binary(pict_path) == pict_binary


def test_write_pict():
    assert PictDBapp.write_pict_from_binary(file_path, pict_binary) == f.write(pict_binary)


def test_make_dir():
    assert PictDBapp.make_dir_if_it_is_not_exists(container_tmp_dir) == directory


def test_add_element():
    assert PictDBapp.add_element_to_list_widget(item_name, list_widget, icon) == list_widget.addItem(item)


def test_buttons_in_work():
    assert PictDBapp.adjust_buttons_in_work_with_pictures_main_window(self) == self.pushButton_load_image.clicked.connect(self.load_image_to_db)


def test_buttons_in_work1():
    assert PictDBapp.adjust_buttons_in_work_with_pictures_main_window(self) == self.pushButton_export_image.clicked.connect(self.unload_image_from_sql)


def test_main():
    assert PictDBapp.main_application_sql() == main.show()


def test__init__():
    assert PictDBapp.__init__(self) == adjust_buttons_in_work_with_pictures_main_window(self)


def test_choose_picture():
    assert PictDBapp.choose_picture_dialog_open(self) == self.label_for_pic.setPixmap(pixmap_resize)


def test_default():
    assert PictDBapp.by_default_setup(self) == self.pict_export_lineEdit.setText('.\\test_data\\some_pict.jpg')


def test_load_imag():
    assert PictDBapp.load_image_to_db(self) == sqlite3_simple_pict_import(data_base, table, pict_path, record_id, description)


def test_show_images():
    assert PictDBapp.show_images_stored_in_database(self) == add_element_to_list_widget('#'+str(pict_specs[0])+'. '+str(pict_specs[1]), self.listWidget_images, temporary_path)


def test_unload_image():
    assert PictDBapp.unload_image_from_sql(self) == export_pict_from_sql(data_base, table, record_id, path)


